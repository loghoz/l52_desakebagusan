-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2018 at 01:06 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kebagusan`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `judul`, `deskripsi`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Banner Kesatu', 'Deskripsi Banner Kedua', 'slider1.jpg', '2018-10-13 17:00:00', '2018-10-20 09:32:06'),
(2, 'Benner Kedua', 'Deskripsi Banner Kedua', 'slider2.jpg', '2018-10-13 17:00:00', '2018-10-13 17:00:00'),
(3, 'Banner Ketiga', 'Deskripsi Banner Ketiga', 'slider3.jpg', '2018-10-13 17:00:00', '2018-10-13 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `beritas`
--

CREATE TABLE `beritas` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `beritas`
--

INSERT INTO `beritas` (`id`, `judul`, `isi`, `photo`, `created_at`, `updated_at`) VALUES
(2, 'Desa Kebagusan Desa Kebagusan Desa Kebagusan Desa Kebagusan Desa Kebagusan', '<p>\r\n\r\n</p><p><b>India dan Nepal</b>&nbsp;tertarik untuk menerapkan program Dana Desa yang diterapkan pemerintah Indonesia, sebagai model altern<i>atif pembangunan desa d</i>i negaranya. Program Dana Desa merupakan program mendanai pembangun<u>an dan pemberdayaan ma</u>syarakat.</p><p>Staf Khus<small>us Kepresidenan Ahmad Erani Y</small>ustika menyatakan, India dan Nepal tertarik dengan kepercayaan pemerintah untuk memberikan anggaran pada level desa. “Tidak seperti dulu, ada pembangunan di desa tetapi anggaran di kabupaten<b>&nbsp;atau pemerintah pu</b>sat,” ungkap dia di Jakarta, Rabu (19/9/2018). <br></p><p>Di samping<b>&nbsp;itu, perumusan dan pelaksanaan program pun dilakukan oleh desa dengan mengandalkan swakelola masyarakat. Program ini dian</b>ggap memunculkan kembali keswadayaan masyarakat. </p>\r\n\r\n<br><p></p>', 'bPp54Qb9dGG2aF6qO0Edgoq4wZECp1Dd2J3nnozp.jpeg', '2018-09-29 09:11:23', '2018-10-12 00:31:15'),
(3, 'Desa Kebagusan Desa Kebagusan Desa Kebagusan Desa Kebagusan Desa Kebagusan', '<p>Desa Kebagusan&nbsp;\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n</p><p>\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n\r\n\r\nDesa Kebagusan \r\n\r\n<br></p>', '-', '2018-09-29 23:09:25', '2018-10-12 00:30:57'),
(4, 'Peningkatan sampah di kabupaten pesawaran Peningkatan sampah di kabupaten pesawaran', '<p>Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran<br></p>', '-', '2018-10-07 20:00:45', '2018-10-07 20:00:45'),
(5, 'Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran', '<p>Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran<br></p>', '-', '2018-10-07 20:00:58', '2018-10-07 20:00:58'),
(6, 'Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran', '<p>Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran<br></p>', '-', '2018-10-07 20:01:04', '2018-10-07 20:01:04'),
(7, 'Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran', '<p>Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran<br></p>', '-', '2018-10-07 20:01:10', '2018-10-07 20:01:10'),
(8, 'Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran', '<p>Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran<br></p>', '-', '2018-10-07 20:01:17', '2018-10-07 20:01:17'),
(9, 'Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran', '<p>Peningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaranPeningkatan sampah di kabupaten pesawaran<br></p>', '-', '2018-10-07 20:01:36', '2018-10-07 20:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `b_p_ds`
--

CREATE TABLE `b_p_ds` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_p_ds`
--

INSERT INTO `b_p_ds` (`id`, `jabatan_id`, `jabatan`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 9, '', 'Erwin Dianto', '', '2018-09-29 06:58:12', '2018-09-29 06:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'aOIfLrldT39lZTEf2APMtkXrE7xLCm1wJQcrteMB.jpeg', '2018-09-29 23:11:55', '2018-09-29 23:11:55'),
(2, 'hAz5dXxS5NghXOnTeYMtQpe6rSDnURLzsJHYatJS.jpeg', '2018-09-29 23:33:18', '2018-09-29 23:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'Kepala Desa', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(2, 'Sekretaris Desa', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(3, 'Kepala Urusan', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(4, 'Kepala Seksi', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(5, 'Staf Sekretariat', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(6, 'Operator Desa', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(7, 'Kepala Dusun', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(8, 'Ketua RT', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(9, 'Ketua', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(10, 'Wakil Ketua', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(11, 'Sekretaris', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(12, 'Bendahara', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(13, 'Seksi', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(14, 'Anggota', '2018-09-29 04:12:42', '2018-09-29 04:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `komentars`
--

CREATE TABLE `komentars` (
  `id` int(10) UNSIGNED NOT NULL,
  `berita_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `komentar` text COLLATE utf8_unicode_ci NOT NULL,
  `balasan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `komentars`
--

INSERT INTO `komentars` (`id`, `berita_id`, `nama`, `email`, `komentar`, `balasan`, `created_at`, `updated_at`) VALUES
(9, 9, 'Erwin Dianto', 'dplh.pesawaran@gmail.com', 'bvcbvcb', '', '2018-10-20 08:42:27', '2018-10-20 08:42:27'),
(10, 9, 'Taufik Nurhuda', 'desa.kebagusan@gmail.com', 'bjmbmbnmnbmbn', 'jghghjgh', '2018-10-20 09:00:11', '2018-10-20 09:02:54');

-- --------------------------------------------------------

--
-- Table structure for table `kontaks`
--

CREATE TABLE `kontaks` (
  `id` int(10) UNSIGNED NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kontaks`
--

INSERT INTO `kontaks` (`id`, `instagram`, `facebook`, `email`, `telp`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'https://www.instagram.com/erwindianto/', 'https://www.facebook.com/erwin.dianto8', 'desa.kebagusan@gmail.com', '072194092', 'Jl. Suro Amijoyo Kampung Sawah Kebagusan Kode POS. 35371', '2018-09-29 17:00:00', '2018-09-29 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `l_p_m_ds`
--

CREATE TABLE `l_p_m_ds` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `l_p_m_ds`
--

INSERT INTO `l_p_m_ds` (`id`, `jabatan_id`, `jabatan`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 9, '', 'Erwin Dianto', '', '2018-09-29 07:01:36', '2018-09-29 07:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

CREATE TABLE `metas` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(1, 'Visi', '<p>\r\n\r\nadalah suatu gambaran yang menantang tentang keadaan masa depan yang diinginkan dengan melihat potensi dan kebutuhan desa. Penyusunan Visi Desa Kebagusan ini dilakukan dengan pendekatan partisipatif, melibatkan pihak-pihak yang berkepentingan di Desa Kebagusan seperti pemerintah Desa, BPD, Tokoh Masyarakat, tokoh agama, lembaga masyarakat desa dan masyarakat desa pada umumnya. Pertimbangan kondisi eksternal di desa seperti satuan kerja wilayah pembangunan di Kecamatan Gedongtataan. Maka berdasarkan pertimbangan diatas Visi Desa Kebagusan adalah : <b>“Menjadikan Desa Kebagusan Lebih Maju Secara Fisik, Pertanian, Ekonomi, Agama, Sosial dan Budaya di Kecamatan Gedongtataan”</b>\r\n\r\n<br></p>', '2018-09-29 21:34:22', '2018-09-29 21:34:22'),
(2, 'Misi', '<p>\r\n\r\nSelain Penyusunan Visi juga telah ditetapkan misi-misi yang memuat sesuatu pernyataan yang harus dilaksanakan oleh Desa agar tercapainya visi desa tersebut.Visi berada di atas Misi .Pernyataan Visi kemudian dijabarkan ke dalam misi agar dapat di operasionalkan / dikerjakan. Sebagaimana penyusunan Visi, misipun dalam penyusunannya menggunakan pendekatan partisipatif dan pertimbangan potensi dan kebutuhan Desa Kebagusan, sebagaiman proses yang dilakukan maka misi Desa Kebagusan adalah :<br></p><blockquote>1. Menggiatkan Kembali Gotong-Royong.<br>2. Meningkatkan Sarana dan Prasarana Fisik (Jalan dan Gedung).<br>3. Meningkatkan Sarana dan Prasarana Pertanian.<br>4. Membuka Peluang Investor Masuk Desa.<br>5. Mengaktifkan Kegiatan Kepemudaan, RISMA.<br>6. Mengaktifkan Kegiatan Poktan, Perikanan, dan Ekonomi Lainnya.<br>7. Membentuk dan Menggiatkan Koperasi di Desa.<br>8. Menggiatkan Pengajian dan Majelis Ta’lim.<br>9. Mengaktifkan Kegiatan Forum Kesehatan Masyarakat Desa.<br>10. Menggiatkan kembali PKK Desa.<br>11. Membentuk dan Mengaktifkan Kelompok Sanimas, Rukem, Pengurus Makam, Pengurus Air Bersih dan lainnya.&nbsp;</blockquote><p></p>', '2018-09-29 21:36:10', '2018-09-29 22:14:08'),
(3, 'Sejarah', '<p>\r\n\r\nPada mulanya Desa Kebagusan merupakan bagian dari Desa Karang Anyar, yang pada tahun 1959 disebut dengan susukan Kebagusan. Selanjutnya tahun 1960 memisahkan diri dari Desa Karang Anyar dengan kades A. Sastro Rejo. Tahun 1965 setelah gestapu kembali menggabungkan diri dengan Desa Karang Anyar yang waktu itu dipimpin Kades Hadi Sumarto, tahun 1968 Kades A.Hakim s.d tahun 1984. Pada tahun tersebut diadakan pemekaran desa dan sebagai penjabat Kades Bapak Aliesan dengan sebutan Desa Kebagusan Kecamatan Tanjung Bintang Kabupaten Lampung Selatan dan pada tanggal 20 Oktober 1986 ditetapkan menjadi Desa Persiapan dengan Kepala Desa Persiapan diangkat Sdr. Aliesan selama 5 tahun. Desa Kebagusan Kecamatan Tanjung Bintang definitif menjadi Desa Kebagusan tahun 1991; dan tahun 1992 diadakan Pilkades awal terpilih lagi Sdr. Aliesan hingga tahun 2002. Pada Januari 2002 habis masa jabatannya dan ditunjuk kembali sebagai Pjs. selama 18 Bulan, selanjutnya mengundurkan diri pada tanggal 14 September 2003.<br><br>Sebagai pengganti beliau ditunjuk Sdr. Pariman sebagai Pjs. Kades Desa Kebagusan beberapa kali hingga akhir tahun 2006. Selanjutnya Sdr. Suparno diangkat sebagai Pjs. Pada tanggal 22 November dan berakhir pada 24 Januari 2007 sekaligus pelatikan Sdr. Ir. Zoehery Zoel sebagai Kades terpilih pada tanggal 28 Desember 2006, menjabat selama 6 tahun sampai dengan tanggal 24 Januari 2013. Sebelum masa jabatan habis, pada tahun 2009 Sdr. Ir. Zoehery Zul cuti untuk mengikuti Pemilu Legislatif, dan jabatan kades diemban oleh Sdr. Solichen, S.Sos selama 8 bulan sampai dengan awal tahun 2010 dan kembali dijabat Ir. Zoehery Zul karena belum berhasil dalam pemilu legilatif sampai dengan masa jabatan habis 24 Januari 2013.<br><br>Tepatnya tanggal 15 Februari 2013 Solichen, S.Sos kembali dilantik menjadi Penjabat Kepala Desa untuk melaksanakan pemilu kades yang berhasil dilaksanakan pada tanggal 12 Mei 2013 dan terpilih Sdr. Sucipto yang dilantik pada 24 Juni 2013 menjabat 6 tahun sd 24 Juni 2019.\r\n\r\n<br></p>', '2018-09-29 21:38:10', '2018-09-29 21:38:10'),
(4, 'Perizinan', '<p>\r\n\r\n</p><h3><b>Surat Pindah</b></h3><h4><b>Dalam satu Desa/Kelurahan</b></h4><p>- Mengisi dan menandatangani formulir permohonan pindah dengan formulir model F-1.08.<br>- Melampirkan KK dan KTP.<br>- Kepala Desa/Lurah menandatangani Surat Keterangan Pindah Datang atas nama Kepala Dinas Kependudukan dan Catatan Sipil.<br></p><h4><b>Antar Desa/Kelurahan dalam satu Kecamatan</b></h4><p>Ditempat Asal :<br>- Mengisi dan menandatangani formulir permohonan pindah dengan formulir model F-1.08;<br>- Melampirkan KK dan KTP;<br>- Kepala Desa/Lurah mengetahui dan membubuhkan tandatangan.<br><br>Ditempat Tujuan :<br>- Penduduk melaporkan kedatangan kepada Kepala Desa/Lurah tempat tujuan dengan menunjukkan Surat Keterangan Pindah dari tempat asal.<br>- Mengisi dan menandatangani formulir pindah datang.<br>- Kepala Desa/Lurah menandatangani Surat Keterangan Pindah Datang.</p><h4><b>Antar Kecamatan dalam Kabupaten</b></h4><p>Ditempat asal :<br>- Mengisi dan menandatangani formulir permohonan pindah dengan formulir model F-1.08.<br>- Melampirkan KK dan KTP.<br>- Kepala Desa/Lurah mengetahui dan membubuhkan tandatangan.<br>- Camat menandatangani surat keterangan pindah atas nama kepala Dinas.<br><br>Ditempat tujuan :<br>- Penduduk melaporkan kedatangannya kepada Kepala Desa/Lurah tempat tujuan dengan menunjukkan Surat Keterangan Pindah dari tempat asal.<br>- Mengisi dan menandatangani formulir pindah datang.<br>- Kepala Desa/Lurah menandatangani Surat Keterangan Pindah Datang dan meneruskan formulir permohonan pindah datang kepada Camat.<br>- Camat menandatangani Surat Keterangan Pindah Datang atas nama Kepala Dinas Kependudukan dan Catatan Sipil, dengan menembuskan kepada Dinas Kependudukan dan Catatan Sipil.<br>- Dinas Kependudukan Catatan Sipil melakukan penarikan terhadap KK dan KTP yang bersangkutan untuk diadakan pergantian sesuai dengan tempat domisili yang baru.</p><h4><b>Antar Kabupaten /Propinsi</b></h4><p>- Mengisi dan menandatangani formulir permohonan pindah.<br>- Melampirkan KK dan KTP.<br>- Kepala Desa/Lurah dan Camat menandatangani formulir permohonan pindah sebagai dasar penerbitan Surat Keterangan Pindah oleh Kepala Dinas.<br>- Dinas Kependudukan dan Catatan Sipil menerbitkan Surat Keterangan Pindah serta menyerahkan kepada penduduk.</p>\r\n\r\n<br><p></p>', '2018-09-29 21:44:43', '2018-09-29 22:07:19'),
(5, 'Kartu Keluarga', '<p>\r\n\r\nKartu Keluarga adalah Kartu Identitas Keluarga yang memuat data tentang susunan, hubungan dan jumlah anggota keluarga. Kartu Keluarga wajib dimiliki oleh setiap keluarga. Kartu ini berisi data lengkap tentang identitas Kepala Keluarga dan anggota keluarganya.&nbsp;</p>', '2018-09-29 21:45:33', '2018-09-29 21:45:33'),
(6, 'Batas Utara Desa ', '<p>\r\n\r\nTanjung Rejo\r\n\r\n<br></p>', '2018-09-29 21:51:48', '2018-09-29 21:51:48'),
(7, 'Batas Timur Desa ', '<p>\r\n\r\nWiyono<br></p>', '2018-09-29 21:52:09', '2018-09-29 21:52:09'),
(8, 'Batas Selatan Desa ', '<p>\r\n\r\nGunung Betung\r\n\r\n<br></p>', '2018-09-29 21:52:25', '2018-09-29 21:52:25'),
(9, 'Batas Barat Desa', '<p>\r\n\r\nSukaraja\r\n\r\n<br></p>', '2018-09-29 21:52:42', '2018-09-29 21:52:42'),
(10, 'Batas Utara Kecamatan', '<p>\r\n\r\nNegeri Katon<br></p>', '2018-09-29 21:53:13', '2018-09-29 21:53:13'),
(11, 'Batas Timur Kecamatan', '<p>\r\n\r\nGedong Tataan<br></p>', '2018-09-29 21:53:37', '2018-09-29 21:53:37'),
(12, 'Batas Selatan Kecamatan', '<p>\r\n\r\nGedong Tataan\r\n\r\n<br></p>', '2018-09-29 21:53:53', '2018-09-29 21:53:53'),
(13, 'Batas Barat Kecamatan', '<p>\r\n\r\nGedong Tataan\r\n\r\n<br></p>', '2018-09-29 21:54:03', '2018-09-29 21:54:03'),
(14, 'Luas Pemukiman', '<p>76 Ha</p>', '2018-09-29 21:54:58', '2018-09-29 21:54:58'),
(15, 'Luas Persawahan', '<p>78 Ha</p>', '2018-09-29 21:55:24', '2018-09-29 22:02:20'),
(16, 'Luas Perkebunan', '<p>721,5 Ha</p>', '2018-09-29 21:55:50', '2018-09-29 21:55:50'),
(17, 'Luas Kuburan', '<p>2,5 Ha</p>', '2018-09-29 21:56:05', '2018-09-29 21:56:05'),
(18, 'Luas Pekarangan', '<p>- Ha</p>', '2018-09-29 21:56:24', '2018-09-29 21:56:24'),
(19, 'Luas Taman', '<p>- Ha</p>', '2018-09-29 21:56:32', '2018-09-29 21:56:32'),
(20, 'Luas Perkantoran', '<p>300 m2</p>', '2018-09-29 21:56:54', '2018-09-29 21:56:54'),
(21, 'Luas Prasarana Umum', '<p>119 Ha</p>', '2018-09-29 21:57:14', '2018-09-29 21:57:14'),
(22, 'Total Luas', '<p>1000 Ha/m2</p>', '2018-09-29 21:57:28', '2018-09-29 21:57:28'),
(23, 'Jarak Ke Ibu Kota Kecamatan', '<p>4 KM</p>', '2018-09-29 21:58:31', '2018-09-29 21:58:31'),
(24, 'Lama jarak tempuh ke ibu kota kecamatan', '<p>5 Menit</p>', '2018-09-29 21:58:50', '2018-09-29 21:58:50'),
(25, 'Jarak Ke Ibu Kabupaten', '<p>\r\n\r\n9 KM\r\n\r\n<br></p>', '2018-09-29 21:59:01', '2018-09-29 21:59:01'),
(26, 'Jarak Ke Ibu Kota Kecamatan', '<p>15 Menit</p>', '2018-09-29 21:59:16', '2018-09-29 21:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2018_09_29_073434_create_beritas_table', 1),
('2018_09_29_073449_create_galleries_table', 1),
('2018_09_29_073458_create_pengaduans_table', 1),
('2018_09_29_073529_create_staff_table', 1),
('2018_09_29_091421_create_jabatans_table', 1),
('2018_09_29_093659_add_column_jabatan_id', 1),
('2018_09_29_105547_create_b_p_ds_table', 1),
('2018_09_29_105602_create_l_p_m_ds_table', 1),
('2018_09_29_105620_create_pkks_table', 1),
('2018_09_29_105644_create_r_ts_table', 1),
('2018_09_29_105701_create_tarunas_table', 1),
('2018_09_30_033328_create_kontaks_table', 2),
('2018_09_30_041341_create_metas_table', 3),
('2018_10_14_021550_create_banners_table', 4),
('2018_10_20_130640_create_komentars_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengaduans`
--

CREATE TABLE `pengaduans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pesan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pkks`
--

CREATE TABLE `pkks` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pkks`
--

INSERT INTO `pkks` (`id`, `jabatan_id`, `jabatan`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 9, '', 'Erwin Dianto', '', '2018-09-29 07:04:08', '2018-09-29 07:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `r_ts`
--

CREATE TABLE `r_ts` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rw` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `r_ts`
--

INSERT INTO `r_ts` (`id`, `jabatan_id`, `jabatan`, `rw`, `rt`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 7, 'I', 'RW.01', '', 'Sobirin', 'Kadus Kebagusan I', '2018-09-29 06:31:39', '2018-09-29 06:33:04'),
(2, 8, '', 'RW.01', 'RT.01', 'Eki Yulindra', '', '2018-09-29 06:33:31', '2018-09-29 06:33:31'),
(3, 7, 'II', 'RW.02', '', 'Jamaludin', 'Kadus Kebagusan II', '2018-09-29 06:34:30', '2018-09-29 06:34:30'),
(4, 8, '', 'RW.02', 'RT.01', 'Winarto', '', '2018-09-29 06:35:05', '2018-09-29 06:35:05'),
(5, 8, '', 'RW.01', 'RT.02', 'Erni Yurita', '', '2018-09-29 06:36:50', '2018-09-29 06:36:50'),
(6, 8, '', 'RW.01', 'RT.03', 'Hidirman', '', '2018-09-29 06:42:04', '2018-09-29 06:42:04'),
(7, 8, '', 'RW.01', 'RT.04', 'Idrus', '', '2018-09-29 06:42:27', '2018-09-29 06:42:27'),
(8, 8, '', 'RW.02', 'RT.02', 'Misrak Nawawi', '', '2018-09-29 06:43:05', '2018-09-29 06:43:05'),
(9, 8, '', 'RW.02', 'RT.03', 'Zubaidi', '', '2018-09-29 06:43:22', '2018-09-29 06:43:22'),
(10, 8, '', 'RW.02', 'RT.04', 'Sutrisman', '', '2018-09-29 06:43:38', '2018-09-29 06:44:30'),
(11, 7, 'III', 'RW.03', '', 'Sumarjo', 'Waylayap I', '2018-09-29 06:45:59', '2018-09-29 06:45:59');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `jabatan_id`, `nama`, `jabatan`, `sk`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tohir, SE.', '', '112/122/2018', '2018-09-29 04:12:42', '2018-09-29 23:11:18'),
(2, 2, 'Budi Cahya Ningrat', '', '', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(3, 3, 'Dwi Sumartini Siwi', 'Kesra', '', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(4, 3, 'Dwi Puspitasari', 'Keuangan', '', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(5, 3, 'M. Idrus', 'Perencanaan', '', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(6, 4, 'Meti Destriani', 'Pemerintahan', '', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(7, 4, 'Dedi Setiadi', 'Pelayanan', '', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(8, 4, 'Joko Suryo T.', 'Kesejahteraan', '', '2018-09-29 04:12:42', '2018-09-29 04:12:42'),
(9, 7, 'Sobirin', 'Kebagusan I', '', '2018-09-29 06:38:05', '2018-09-29 06:38:05'),
(10, 7, 'Jamaludin', 'Kebagusan II', '', '2018-09-29 06:38:31', '2018-09-29 06:38:31'),
(11, 7, 'Sumarjo', 'Waylayap I', '', '2018-09-29 06:38:48', '2018-09-29 06:38:48'),
(12, 7, 'Sri Murti', 'Waylayap II', '', '2018-09-29 06:39:05', '2018-09-29 06:39:05'),
(13, 7, 'Aan Rusmayati', 'Sidototo', '', '2018-09-29 06:39:31', '2018-09-29 06:39:31'),
(14, 7, 'Tri Yuliani', 'Kampung Sawah', '', '2018-09-29 06:39:50', '2018-09-29 06:39:50'),
(15, 7, 'Subandi', 'Triharjo', '', '2018-09-29 06:40:14', '2018-09-29 06:40:14'),
(16, 7, 'Suprapto', 'Way Berulu', '', '2018-09-29 06:40:36', '2018-09-29 06:40:36');

-- --------------------------------------------------------

--
-- Table structure for table `tarunas`
--

CREATE TABLE `tarunas` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tarunas`
--

INSERT INTO `tarunas` (`id`, `jabatan_id`, `jabatan`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 9, '', 'Erwin Dianto', '', '2018-09-29 07:10:34', '2018-09-29 07:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'desa.kebagusan@gmail.com', '$2y$10$cHl80VpU2YUbronCZjKpruei5OY2ffAEJ19oBLrK.dMAf4F62wlsW', 'pgRmTuQzTgf6tfoMpWCGapmraJTcWqY5wTLaYHUMQOeN64klLSFIgCRfq9FM', '2018-09-29 04:12:42', '2018-09-29 23:34:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beritas`
--
ALTER TABLE `beritas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_p_ds`
--
ALTER TABLE `b_p_ds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b_p_ds_jabatan_id_foreign` (`jabatan_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komentars`
--
ALTER TABLE `komentars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `komentars_berita_id_foreign` (`berita_id`);

--
-- Indexes for table `kontaks`
--
ALTER TABLE `kontaks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `l_p_m_ds`
--
ALTER TABLE `l_p_m_ds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `l_p_m_ds_jabatan_id_foreign` (`jabatan_id`);

--
-- Indexes for table `metas`
--
ALTER TABLE `metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pengaduans`
--
ALTER TABLE `pengaduans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pkks`
--
ALTER TABLE `pkks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pkks_jabatan_id_foreign` (`jabatan_id`);

--
-- Indexes for table `r_ts`
--
ALTER TABLE `r_ts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `r_ts_jabatan_id_foreign` (`jabatan_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staff_jabatan_id_foreign` (`jabatan_id`);

--
-- Indexes for table `tarunas`
--
ALTER TABLE `tarunas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tarunas_jabatan_id_foreign` (`jabatan_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `beritas`
--
ALTER TABLE `beritas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `b_p_ds`
--
ALTER TABLE `b_p_ds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `komentars`
--
ALTER TABLE `komentars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kontaks`
--
ALTER TABLE `kontaks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `l_p_m_ds`
--
ALTER TABLE `l_p_m_ds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `metas`
--
ALTER TABLE `metas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `pengaduans`
--
ALTER TABLE `pengaduans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pkks`
--
ALTER TABLE `pkks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `r_ts`
--
ALTER TABLE `r_ts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tarunas`
--
ALTER TABLE `tarunas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `b_p_ds`
--
ALTER TABLE `b_p_ds`
  ADD CONSTRAINT `b_p_ds_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `komentars`
--
ALTER TABLE `komentars`
  ADD CONSTRAINT `komentars_berita_id_foreign` FOREIGN KEY (`berita_id`) REFERENCES `beritas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `l_p_m_ds`
--
ALTER TABLE `l_p_m_ds`
  ADD CONSTRAINT `l_p_m_ds_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pkks`
--
ALTER TABLE `pkks`
  ADD CONSTRAINT `pkks_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `r_ts`
--
ALTER TABLE `r_ts`
  ADD CONSTRAINT `r_ts_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tarunas`
--
ALTER TABLE `tarunas`
  ADD CONSTRAINT `tarunas_jabatan_id_foreign` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatans` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
