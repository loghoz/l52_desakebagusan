<?php

use Illuminate\Database\Seeder;
use App\Jabatan;
class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //sample jabatan
        Jabatan::create(['jabatan' => 'Kepala Desa']);
        Jabatan::create(['jabatan' => 'Sekretaris Desa']);
        Jabatan::create(['jabatan' => 'Kepala Urusan']);
        Jabatan::create(['jabatan' => 'Kepala Seksi']);
        Jabatan::create(['jabatan' => 'Staf Sekretariat']);
        Jabatan::create(['jabatan' => 'Operator Desa']);
        Jabatan::create(['jabatan' => 'Kepala Dusun']);
        Jabatan::create(['jabatan' => 'Ketua RT']);
        Jabatan::create(['jabatan' => 'Ketua']);
        Jabatan::create(['jabatan' => 'Wakil Ketua']);
        Jabatan::create(['jabatan' => 'Sekretaris']);
        Jabatan::create(['jabatan' => 'Bendahara']);
        Jabatan::create(['jabatan' => 'Seksi']);
        Jabatan::create(['jabatan' => 'Anggota']);
    }
}
