<?php

use Illuminate\Database\Seeder;
use App\Staff;
class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Staff::create([
            'jabatan_id' => 1,
            'jabatan' => "",
            'nama' => "Tohir, SE."
        ]);

        Staff::create([
            'jabatan_id' => 2,
            'jabatan' => "",
            'nama' => "Budi Cahya Ningrat"
        ]);

        Staff::create([
            'jabatan_id' => 3,
            'jabatan' => "Kesra",
            'nama' => "Dwi Sumartini Siwi"
        ]);

        Staff::create([
            'jabatan_id' => 3,
            'jabatan' => "Keuangan",
            'nama' => "Dwi Puspitasari"
        ]);

        Staff::create([
            'jabatan_id' => 3,
            'jabatan' => "Perencanaan",
            'nama' => "M. Idrus"
        ]);

        Staff::create([
            'jabatan_id' => 4,
            'jabatan' => "Pemerintahan",
            'nama' => "Meti Destriani"
        ]);

        Staff::create([
            'jabatan_id' => 4,
            'jabatan' => "Pelayanan",
            'nama' => "Dedi Setiadi"
        ]);

        Staff::create([
            'jabatan_id' => 4,
            'jabatan' => "Kesejahteraan",
            'nama' => "Joko Suryo T."
        ]);
    }
}
