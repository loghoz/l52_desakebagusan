<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //sample admin
        App\User::create([
            'email' => 'desa.kebagusan@gmail.com',
            'name' => 'Admin',
            'password' => bcrypt('1sampai9')
        ]);
    }
}
