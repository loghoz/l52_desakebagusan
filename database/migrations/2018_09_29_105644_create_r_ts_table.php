<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRTsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_ts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jabatan_id')->unsigned();
            $table->string('jabatan');
            $table->string('rw');
            $table->string('rt');
            $table->string('nama');
            $table->string('keterangan');
            $table->timestamps();

            $table->foreign('jabatan_id')
                ->references('id')
                ->on('jabatans')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('r_ts');
    }
}
