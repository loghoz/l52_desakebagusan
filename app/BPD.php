<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BPD extends Model
{
    protected $table = 'b_p_ds';

    protected $fillable = [
        'jabatan_id','jabatan','nama','keterangan'
    ];

     // relasi ke jabatan
     public function jab()
     {
         return $this->belongsTo('App\Jabatan', 'jabatan_id');
     }
}
