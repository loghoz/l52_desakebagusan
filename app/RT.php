<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RT extends Model
{
    protected $table = 'r_ts';
    protected $fillable = [
        'jabatan_id','jabatan','rw','rt','nama','keterangan'
    ];

     // relasi ke jabatan
     public function jab()
     {
         return $this->belongsTo('App\Jabatan', 'jabatan_id');
     }
}
