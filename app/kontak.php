<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kontak extends Model
{
    protected $table = 'kontaks';

    protected $fillable = [
        'instagram','facebook','telp','alamat','email'
    ];
}
