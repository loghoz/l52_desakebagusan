<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatans';

    protected $fillable = [
        'jabatan'
    ];

    // relasi one to many ke staff
    public function staff()
    {
        return $this->hasMany('App\Staff');
    }

    // relasi one to many ke staff
    public function bpd()
    {
        return $this->hasMany('App\BPD');
    }

    // relasi one to many ke staff
    public function lpmd()
    {
        return $this->hasMany('App\LPMD');
    }

    // relasi one to many ke staff
    public function pkk()
    {
        return $this->hasMany('App\pkk');
    }

    // relasi one to many ke staff
    public function rt()
    {
        return $this->hasMany('App\RT');
    }
    // relasi one to many ke staff
    public function taruna()
    {
        return $this->hasMany('App\Taruna');
    }
}
