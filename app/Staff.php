<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';

    protected $fillable = [
        'jabatan_id','jabatan','nama','sk'
    ];

     // relasi ke jabatan
     public function jab()
     {
         return $this->belongsTo('App\Jabatan', 'jabatan_id');
     }
}
