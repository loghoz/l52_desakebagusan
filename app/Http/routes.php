<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::auth();

Route::resource('/', 'User\BerandaController@index');
Route::post('/pengaduan/store', 'User\BerandaController@store');

Route::resource('/profil/visimisi', 'User\ProfilController@visimisi');
Route::resource('/profil/sejarah', 'User\ProfilController@sejarah');
Route::resource('/profil/demografi', 'User\ProfilController@demografi');

Route::resource('/berita', 'User\BeritaController');
Route::get('cari', 'User\BeritaController@cari');

Route::resource('/lembaga/struktur', 'User\StrukturController');
Route::resource('/lembaga/staff', 'User\StaffController');
Route::resource('/lembaga/bpd', 'User\BPDController');
Route::resource('/lembaga/lpmd', 'User\LPMDController');
Route::resource('/lembaga/pkk', 'User\PKKController');
Route::resource('/lembaga/rt', 'User\RTController');
Route::resource('/lembaga/karangtaruna', 'User\KarangTarunaController');

Route::resource('/layanan/perizinan', 'User\LayananController@perizinan');
Route::resource('/layanan/kk', 'User\LayananController@kk');

Route::resource('/gallery', 'User\GalleryController');

//ADMIN
Route::resource('/admin/beranda', 'Admin\WelcomeController');
Route::resource('/admin/berita', 'Admin\BeritaController');
Route::resource('/admin/pengaduan', 'Admin\PengaduanController');
Route::resource('/admin/komentar', 'Admin\KomentarController');
Route::resource('/admin/gallery', 'Admin\GalleryController');

Route::resource('/admin/lembaga/staff', 'Admin\StaffController');
Route::resource('/admin/lembaga/bpd', 'Admin\BPDController');
Route::resource('/admin/lembaga/lpmd', 'Admin\LPMDController');
Route::resource('/admin/lembaga/rt', 'Admin\RTController');
Route::resource('/admin/lembaga/karangtaruna', 'Admin\KarangTarunaController');
Route::resource('/admin/lembaga/pkk', 'Admin\PKKController');

Route::resource('/admin/pengaturan', 'Admin\PengaturanController');
Route::resource('/admin/pengaturan/kontak', 'Admin\KontakController');
Route::resource('/admin/info', 'Admin\MetaController');
Route::resource('/admin/banner', 'Admin\BannerController');
