<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\RT;
use App\Jabatan;

class RTController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $no = 1;
        $jabatan = Jabatan::lists('jabatan', 'id');

        $rt = RT::orderBy('rw','ASC')
                    ->orderBy('jabatan_id','ASC')
                    ->get();
        // return view('admin.jurusan.jurusan', compact('jurusan','no'));

        return view('admin.lembaga.rt', compact('rt','jabatan','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        RT::create($request->all());
        $notification = array(
            'message' => 'Data berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.lembaga.rt.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $rt = RT::findOrFail($id);
        $jabatan = Jabatan::lists('jabatan', 'id');

        return view('admin.lembaga.edit_rt', compact('rt','jabatan'));
    }

    public function update(Request $request, $id)
    {
        $rt = RT::findOrFail($id);

        $rt->update($request->all());
        $notification = array(
            'message' => 'Data berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.lembaga.rt.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = RT::findOrFail($id);
        $notification = array(
            'message' => 'Data berhasil dihapus.',
            'alert-type' => 'error'
        );
        RT::find($id)->delete();
        return redirect()->route('admin.lembaga.rt.index')->with($notification);
    }
}
