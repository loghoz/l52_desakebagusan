<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\BPD;
use App\Jabatan;

class BPDController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $no = 1;
        $jabatan = Jabatan::lists('jabatan', 'id');

        $bpd = BPD::orderBy('jabatan_id','ASC')->get();
        // return view('admin.jurusan.jurusan', compact('jurusan','no'));

        return view('admin.lembaga.bpd', compact('bpd','jabatan','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        BPD::create($request->all());
        $notification = array(
            'message' => 'Data berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.lembaga.bpd.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $bpd = BPD::findOrFail($id);
        $jabatan = Jabatan::lists('jabatan', 'id');

        return view('admin.lembaga.edit_bpd', compact('bpd','jabatan'));
    }

    public function update(Request $request, $id)
    {
        $bpd = BPD::findOrFail($id);

        $bpd->update($request->all());
        $notification = array(
            'message' => 'Data berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.lembaga.bpd.index');
    }

    public function destroy($id)
    {
        $hapus = BPD::findOrFail($id);
        $notification = array(
            'message' => 'Data berhasil dihapus.',
            'alert-type' => 'error'
        );
        BPD::find($id)->delete();
        return redirect()->route('admin.lembaga.bpd.index');
    }
}
