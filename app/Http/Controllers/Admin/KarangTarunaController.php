<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Taruna;
use App\Jabatan;
class KarangTarunaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $no = 1;
        $jabatan = Jabatan::lists('jabatan', 'id');

        $taruna = Taruna::orderBy('jabatan_id','ASC')->get();
        // return view('admin.jurusan.jurusan', compact('jurusan','no'));

        return view('admin.lembaga.taruna', compact('taruna','jabatan','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Taruna::create($request->all());
        $notification = array(
            'message' => 'Data berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.lembaga.karangtaruna.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $taruna = Taruna::findOrFail($id);
        $jabatan = Jabatan::lists('jabatan', 'id');

        return view('admin.lembaga.edit_taruna', compact('taruna','jabatan'));
    }

    public function update(Request $request, $id)
    {
        $taruna = Taruna::findOrFail($id);

        $taruna->update($request->all());
        $notification = array(
            'message' => 'Data berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.lembaga.karangtaruna.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Taruna::findOrFail($id);
        $notification = array(
            'message' => 'Data berhasil dihapus.',
            'alert-type' => 'error'
        );
        Taruna::find($id)->delete();
        return redirect()->route('admin.lembaga.karangtaruna.index')->with($notification);
    }
}
