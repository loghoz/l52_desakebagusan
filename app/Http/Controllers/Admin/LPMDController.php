<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\LPMD;
use App\Jabatan;

class LPMDController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $no = 1;
        $jabatan = Jabatan::lists('jabatan', 'id');

        $lpmd = LPMD::orderBy('jabatan_id','ASC')->get();
        // return view('admin.jurusan.jurusan', compact('jurusan','no'));

        return view('admin.lembaga.lpmd', compact('lpmd','jabatan','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        LPMD::create($request->all());
        $notification = array(
            'message' => 'Data berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.lembaga.lpmd.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $lpmd = LPMD::findOrFail($id);
        $jabatan = Jabatan::lists('jabatan', 'id');

        return view('admin.lembaga.edit_lpmd', compact('lpmd','jabatan'));
    }

    public function update(Request $request, $id)
    {
        $lpmd = LPMD::findOrFail($id);

        $lpmd->update($request->all());
        $notification = array(
            'message' => 'Data berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.lembaga.lpmd.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = LPMD::findOrFail($id);
        $notification = array(
            'message' => 'Data berhasil dihapus.',
            'alert-type' => 'error'
        );
        LPMD::find($id)->delete();
        return redirect()->route('admin.lembaga.lpmd.index')->with($notification);
    }
}
