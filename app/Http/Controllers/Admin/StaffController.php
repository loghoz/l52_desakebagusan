<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Staff;
use App\Jabatan;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $no = 1;
        $jabatan = Jabatan::lists('jabatan', 'id');

        $staff = Staff::orderBy('jabatan_id','ASC')->get();
        // return view('admin.jurusan.jurusan', compact('jurusan','no'));

        return view('admin.lembaga.staff', compact('staff','jabatan','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Staff::create($request->all());
        $notification = array(
            'message' => 'Data berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.lembaga.staff.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $staff = Staff::findOrFail($id);
        $jabatan = Jabatan::lists('jabatan', 'id');

        return view('admin.lembaga.edit_staff', compact('staff','jabatan'));
    }

    public function update(Request $request, $id)
    {
        $staff = Staff::findOrFail($id);

        $staff->update($request->all());
        $notification = array(
            'message' => 'Data berhasil diubah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.lembaga.staff.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = Staff::findOrFail($id);
        $notification = array(
            'message' => 'Data berhasil dihapus.',
            'alert-type' => 'error'
        );
        Staff::find($id)->delete();
        return redirect()->route('admin.lembaga.staff.index')->with($notification);
    }
}
