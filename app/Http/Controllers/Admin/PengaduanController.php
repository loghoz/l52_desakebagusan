<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pengaduan;
class PengaduanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 10 - 9;
        }else{
            $no=1;
        }

        $pengaduan = Pengaduan::orderBy('id','DESC')->paginate(10);

        return view('admin.pengaduan.pengaduan',compact('pengaduan','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $hapus = Pengaduan::findOrFail($id);
        Pengaduan::find($id)->delete();
        $notification = array(
            'message' => 'Pengaduan berhasil dihapus.',
            'alert-type' => 'error'
        );
        return redirect()->route('admin.pengaduan.index')->with($notification);
    }
}
