<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\pkk;
use App\Jabatan;

class PKKController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $no = 1;
        $jabatan = Jabatan::lists('jabatan', 'id');

        $pkk = pkk::orderBy('jabatan_id','ASC')->get();
        // return view('admin.jurusan.jurusan', compact('jurusan','no'));

        return view('admin.lembaga.pkk', compact('pkk','jabatan','no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        pkk::create($request->all());
        $notification = array(
            'message' => 'Data berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.lembaga.pkk.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pkk = pkk::findOrFail($id);
        $jabatan = Jabatan::lists('jabatan', 'id');

        return view('admin.lembaga.edit_pkk', compact('pkk','jabatan'));
    }

    public function update(Request $request, $id)
    {
        $pkk = pkk::findOrFail($id);

        $pkk->update($request->all());
        $notification = array(
            'message' => 'Data berhasil diubah.',
            'alert-type' => 'info'
        );
        return redirect()->route('admin.lembaga.pkk.index')->with($notification);
    }

    public function destroy($id)
    {
        $hapus = pkk::findOrFail($id);
        $notification = array(
            'message' => 'Data berhasil dihapus.',
            'alert-type' => 'error'
        );
        pkk::find($id)->delete();
        return redirect()->route('admin.lembaga.pkk.index')->with($notification);
    }
}
