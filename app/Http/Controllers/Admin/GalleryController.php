<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Gallery;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $gallery = Gallery::orderBy('id','DESC')->paginate(8);

        return view('admin.gallery.gallery', compact('gallery'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        if ($request->hasFile('photo')) {
            $gallery['photo'] = $this->savePhoto($request->file('photo'));
        }

        Gallery::create($gallery);

        $notification = array(
            'message' => 'Gambar berhasil ditambah.',
            'alert-type' => 'succes'
        );
        return redirect()->route('admin.gallery.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $hapus = Gallery::findOrFail($id);

        $gallery = Gallery::find($id);
        $this->deletePhoto($gallery->photo);
  
        $notification = array(
            'message' => 'Gambar berhasil dihapus.',
            'alert-type' => 'error'
        );

        Gallery::find($id)->delete();
        return redirect()->route('admin.gallery.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'itlabil/images/gallery';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'itlabil/images/gallery/'.$filename;
      return File::delete($path);
    }
}
