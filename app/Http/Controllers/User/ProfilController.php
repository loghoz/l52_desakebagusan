<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Meta;
use App\kontak;
class ProfilController extends Controller
{

    public function visimisi()
    {
        $visi = Meta::where('id',1)->get()->first();
        $misi = Meta::where('id',2)->get()->first();
        $kontak = kontak::where('id',1)->get()->first();
        return view('user.profil.visimisi', compact('visi','misi','kontak'));
    }

    public function sejarah()
    {
        $kontak = kontak::where('id',1)->get()->first();
        $sejarah = Meta::where('id',3)->get()->first();
        return view('user.profil.sejarah', compact('sejarah','kontak'));
    }
    
    public function demografi()
    {
        $kontak = kontak::where('id',1)->get()->first();
        $bud = Meta::where('id',6)->get()->first();
        $bsd = Meta::where('id',8)->get()->first();
        $btd = Meta::where('id',7)->get()->first();
        $bbd = Meta::where('id',9)->get()->first();
        $buk = Meta::where('id',10)->get()->first();
        $bsk = Meta::where('id',12)->get()->first();
        $btk = Meta::where('id',11)->get()->first();
        $bbk = Meta::where('id',13)->get()->first();

        $lpemukiman = Meta::where('id',14)->get()->first();
        $lpersawahan = Meta::where('id',15)->get()->first();
        $lperkebunan = Meta::where('id',16)->get()->first();
        $lkuburan = Meta::where('id',17)->get()->first();
        $lpekarangan = Meta::where('id',18)->get()->first();
        $ltaman = Meta::where('id',19)->get()->first();
        $lperkantoran = Meta::where('id',20)->get()->first();
        $lprasarana = Meta::where('id',21)->get()->first();
        $ltotal = Meta::where('id',22)->get()->first();

        $lkec = Meta::where('id',23)->get()->first();
        $jkec = Meta::where('id',24)->get()->first();
        $lkab = Meta::where('id',25)->get()->first();
        $jkab = Meta::where('id',26)->get()->first();
        
        return view('user.profil.demografi', compact('bud','bsd','btd','bbd','buk','bsk','btk','bbk'
                                            ,'lpemukiman','lpersawahan','lperkebunan','lkuburan','lpekarangan'
                                            ,'ltaman','lperkantoran','lprasarana','ltotal'
                                            ,'lkec','jkec','lkab','jkab','kontak'
        ));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
