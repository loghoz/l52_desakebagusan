<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LPMD extends Model
{
    protected $table = 'l_p_m_ds';
    protected $fillable = [
        'jabatan_id','jabatan','nama','keterangan'
    ];

     // relasi ke jabatan
     public function jab()
     {
         return $this->belongsTo('App\Jabatan', 'jabatan_id');
     }
}
