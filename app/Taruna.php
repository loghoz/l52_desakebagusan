<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taruna extends Model
{
    protected $table = 'tarunas';
    protected $fillable = [
        'jabatan_id','jabatan','nama','keterangan'
    ];

     // relasi ke jabatan
     public function jab()
     {
         return $this->belongsTo('App\Jabatan', 'jabatan_id');
     }
}
