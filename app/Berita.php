<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'beritas';

    protected $fillable = [
        'judul','isi','photo',
    ];

    // relasi one to many ke komentar
    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
