<div class="col-sm-12">
        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
            {!! Form::label('nama', 'Nama', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-8">
            {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama']) !!}
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('nama') }}</small>
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {!! Form::label('email', 'Email', ['class'=>'control-label col-sm-2']) !!}
            <div class="col-sm-8">
            {!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Email']) !!}
            </div>
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>
        </div>
    <div class="form-group{{ $errors->has('komentar') ? ' has-error' : '' }}">
        {!! Form::label('komentar', 'Komentar', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-8">
            <div class="box-body pad">
                {!! Form::textarea('komentar', null, ['class' => 'textarea','placeholder'=>'Tulis Komentar','style' => 'width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px']) !!}
            </div>
        </div>
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('komentar') }}</small>
        </div>
    </div>
    <input type="hidden" name="berita_id" value="{{ $berita->id }}">
    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Kirim", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
