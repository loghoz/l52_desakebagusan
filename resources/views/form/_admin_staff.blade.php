<div class="col-sm-6">

    <div class="form-group{{ $errors->has('jabatan_id') ? ' has-error' : '' }}">
        {!! Form::label('jabatan_id', 'Jabatan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::select('jabatan_id',$jabatan ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('jabatan_id') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('jabatan') ? ' has-error' : '' }}">
        {!! Form::label('jabatan', ' ', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('jabatan', null, ['class' => 'form-control','placeholder'=>'Jabatan']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('jabatan') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
        {!! Form::label('nama', 'Nama', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('nama') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('sk') ? ' has-error' : '' }}">
        {!! Form::label('sk', 'No SK Pengangkatan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
        {!! Form::text('sk', null, ['class' => 'form-control','placeholder'=>'Contoh : 112/112/2018']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
        <small class="text-danger">{{ $errors->first('sk') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>

</div>