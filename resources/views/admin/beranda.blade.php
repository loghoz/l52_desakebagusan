@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <div align="center"><br><br>
                    <img src="../itlabil/images/default/logo-pesawaran.png" width="200" alt="logo">
                </div>
                <div align="center">
                    <p align="center">
                        <br><br>
                        <font size="4">PEMERINTAHAN KABUPATEN PESAWARAN</font><br>
                        <font size="6">DESA KEBAGUSAN</font><br>
                        <font size="5">KECAMATAN GEDONG TATAAN</font><br>
                        {{$kontak->alamat}}
                    </p>
                </div>

            </div>
        </div>
    </div>
@endsection
