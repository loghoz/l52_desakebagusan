@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Rukun Tetangga
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Data RT</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.lembaga.rt.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_rt')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data RT</h3>
                    </div>
                        <div class="box-body" style="overflow-x:auto;">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td>No</td>
                                    <td>RW</td>
                                    <td>RT</td>
                                    <td>Nama</td>
                                    <td>Jabatan</td>
                                    <td>Keterangan</td>
                                    <td colspan="2" align="center">Action</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($rt as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->rw }}</td>
                                        <td>{{ $item->rt }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->jab->jabatan }} {{$item->jabatan }}</td>
                                        <td>{{ $item->keterangan}}</td>
                                        <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.lembaga.rt.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                            </div>
                                            <div class="col-md-6">
                                                {{ Form::open(['route' => ['admin.lembaga.rt.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                                    {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
