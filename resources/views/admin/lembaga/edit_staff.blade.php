@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Staff
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Data Staff</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($staff, ['route' => ['admin.lembaga.staff.update', $staff],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_staff', ['model' => $staff])
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
