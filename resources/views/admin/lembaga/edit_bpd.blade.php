@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Badan Permusyawaratan Daerah
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Data BPD</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($bpd, ['route' => ['admin.lembaga.bpd.update', $bpd],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_bpd', ['model' => $bpd])
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
