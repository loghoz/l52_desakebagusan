@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Staff
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Data Staff</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.lembaga.staff.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_staff')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Staff</h3>
                    </div>
                        <div class="box-body" style="overflow-x:auto;">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama</td>
                                    <td>Jabatan</td>
                                    <td>No SK Jabatan</td>
                                    <td colspan="2" align="center">Action</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($staff as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->jab->jabatan }} {{$item->jabatan }}</td>
                                        <td>{{ $item->sk}}</td>
                                        <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.lembaga.staff.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                            </div>
                                            <div class="col-md-6">
                                                {{ Form::open(['route' => ['admin.lembaga.staff.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                                    {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
