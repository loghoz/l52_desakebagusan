@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Karang Taruna
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Data Karang Taruna</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($taruna, ['route' => ['admin.lembaga.karangtaruna.update', $taruna],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_bpd', ['model' => $taruna])
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
