@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Komentar
        </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Balas Komentar</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($komentar, ['route' => ['admin.komentar.update', $komentar],'method' =>'patch','class'=>'form-horizontal','files'=>true])!!}
                            @include('form._admin_balas_komentar', ['model' => $komentar])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
