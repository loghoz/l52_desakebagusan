@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Komentar
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Komentar</h3>
                    </div>
                    <div class="box-body" style="overflow-x:auto;">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Judul Berita</td>
                                <td>Pesan</td>
                                <td colspan="2">Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($komentar as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->berita->judul }}</td>
                                    <td>
                                        Nama : {{ $item->nama }}<br>
                                        Email : {{ $item->email }}<br>
                                        <span class="label label-success">{{ $item->created_at->format('h:m') }}</span>
                                        <span class="label label-info">{{ $item->created_at->format('d M Y') }}</span><br><br>
                                        <font color="#4cae4c">Komentar </font>:<br>{{ $item->komentar }}
                                        @if($item->balasan!='')
                                            <br><br>
                                            <font color="#4cae4c">Balas </font>:<br>{{ $item->balasan }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->balasan!='')
                                            <a href="{{ route('admin.komentar.edit', $item->id) }}" class="btn btn-success">Ubah Balasan
                                        @else
                                            <a href="{{ route('admin.komentar.edit', $item->id) }}" class="btn btn-primary">Balas Komentar</a>
                                        @endif
                                    </td>
                                    <td>
                                        {!! Form::model($item, ['route' => ['admin.komentar.destroy', $item], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                        {!! Form::close()!!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $komentar->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
