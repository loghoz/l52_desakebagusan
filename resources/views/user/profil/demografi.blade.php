@extends('layouts.user')

@section('content')
    <section id="layanan" data-stellar-background-ratio="2.5">
        <div class="container">
            <div class="row">
    
                <div class="col-md-12 col-sm-12">
                     <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Demografi</h2>
                    </div>
                </div>
            </div>				
            <div class="row">

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Batas Wilayah</h3><br>
                        </div>

                        <div class="box-body" style="overflow-x:auto;">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Batas</th>
                                        <th>Desa / Kelurahan</th>
                                        <th>Kecamatan</th>
                                    </tr>
                                    <tr>
                                        <td>1.</td>
                                        <td>Sebelah Utara</td>
                                        <td>{!! $bud->meta_value !!}</td>
                                        <td>{!! $buk->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Sebelah Selatan</td>
                                        <td>{!! $bsd->meta_value !!}</td>
                                        <td>{!! $bsk->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Sebelah Timur</td>
                                        <td>{!! $btd->meta_value !!}</td>
                                        <td>{!! $btk->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Sebelah Barat</td>
                                        <td>{!! $bbd->meta_value !!}</td>
                                        <td>{!! $bbk->meta_value !!}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>	

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Luas Wilayah</h3><br>
                        </div>

                        <div class="box-body" style="overflow-x:auto;">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Wilayah</th>
                                        <th>Luas</th>
                                    </tr>
                                    <tr>
                                        <td>1.</td>
                                        <td>Pemukiman</td>
                                        <td>{!! $lpemukiman->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Persawahan</td>
                                        <td>{!! $lpersawahan->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Perkebunan</td>
                                        <td>{!! $lperkebunan->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Kuburan</td>
                                        <td>{!! $lkuburan->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Pekarangan</td>
                                        <td>{!! $lpekarangan->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Taman</td>
                                        <td>{!! $ltaman->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>7.</td>
                                        <td>Perkantoran</td>
                                        <td>{!! $lperkantoran->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>8.</td>
                                        <td>Prasarana Umum</td>
                                        <td>{!! $lprasarana->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <th>9.</th>
                                        <th>Total Luas</th>
                                        <th>{!! $ltotal->meta_value !!}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>	

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Orbitase</h3><br>
                        </div>

                        <div class="box-body" style="overflow-x:auto;">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Jarak Ke Ibu Kota Kecamatan</td>
                                        <td>{!! $lkec->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Lama jarak tempuh ke ibu kota kecamatan</td>
                                        <td>{!! $jkec->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Jarak Ke Ibu Kabupaten</td>
                                        <td>{!! $lkab->meta_value !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Jarak Ke Ibu Kota Kecamatan</td>
                                        <td>{!! $jkab->meta_value !!}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>    					
            </div>
        </div>
    </section>
@endsection
