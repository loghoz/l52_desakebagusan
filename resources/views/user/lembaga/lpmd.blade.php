@extends('layouts.user')

@section('content')
    <section id="layanan" data-stellar-background-ratio="2.5">
        <div class="container">
            <div class="row">
    
                <div class="col-md-12 col-sm-12">
                     <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Lembaga Pemberdayaan Masyarakat Desa (LPMD)</h2>
                    </div>
                </div>
            </div>
            					
            					
            <div class="row">

                <div class="col-lg-12">
                    <h3>Struktur Organisasi dan Susunan Pengurus LPMD</h3><br>
                    <p align="justify">
                        Lembaga Pemberdayaan Masyarakat Desa atau Kelurahan (LPMD/LPMK)/Lembaga Ketahanan Masyarakat Desa atau Kelurahan (LKMDILKMK) atau sebutan nama lain mempunyai tugas menyusun rencana pembangunan secara partisipatif, menggerakkan swadaya gotong royong masyarakat, melaksanakan dan mengendalikan pembangunan.
                    </p>
                    <p align="justify">
                        Lembaga Pemberdayaan Masyarakat Desa atau Kelurahan (LPMD/LPMK)/Lembaga Ketahanan Masyarakat Desa atau Kelurahan (LKMD/LKMK) atau sebutan nama lain dalam<br> melaksanakan tugasnya mempunyai fungsi :
                    </p>
                    <p align="justify">
                        1. Penampungan dan penyaluran aspirasi masyarakat dalam pembangunan.<br>
                        2. Penanaman dan pemupukan rasa persatuan dan kesatuan masyarakat dalam kerangka memperkokoh Negara Kesatuan Republik Indonesia.<br>
                        3. Peningkatan kualitas dan percepatan pelayanan pemerintah kepada masyarakat.<br>
                        4. Penyusunan rencana, pelaksanaan, pelestarian dan pengembangan hasil-hasil pembangunan secara partisipatif.<br>
                        5. Penumbuhkembangan dan penggerak prakarsa, partisipasi, serta swadaya gotong royong masyarakat dan.<br>
                        6. Penggali, pendayagunaan dan pengembangan potensi sumber daya alam serta keserasian lingkungan hidup.					
                    </p>
                </div>	

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pengurus LPMD</h3><br>
                        </div>

                        <div class="box-body" style="overflow-x:auto;">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Keterangan</th>
                                    </tr>
                                    @foreach($staff as $item)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $item->nama }}</td>
                                            <td>{{ $item->jab->jabatan }} {{$item->jabatan }}</td>
                                            <td>{{ $item->keterangan}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>	      					
            </div>
        </div>
    </section>
@endsection
