@extends('layouts.user')

@section('content')
    <section id="layanan" data-stellar-background-ratio="2.5">
        <div class="container">
            <div class="row">
    
                <div class="col-md-12 col-sm-12">
                     <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Karang Taruna</h2>
                    </div>
                </div>
            </div>
            					
            <div class="row">

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pengurus Karang Taruna</h3><br>
                        </div>

                        <div class="box-body" style="overflow-x:auto;">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">No</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Keterangan</th>
                                    </tr>
                                    @foreach($staff as $item)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $item->nama }}</td>
                                            <td>{{ $item->jab->jabatan }} {{$item->jabatan }}</td>
                                            <td>{{ $item->keterangan}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>	      					
            </div>
        </div>
    </section>

@endsection
