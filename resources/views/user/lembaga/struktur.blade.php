@extends('layouts.user')

@section('content')
	<section id="layanan" data-stellar-background-ratio="2.5">
        <div class="container">
            <div class="row">
    
                <div class="col-md-12 col-sm-12">
                     <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Struktur</h2>
                    </div>
                </div>
            </div>				
			<div class="row">

				<div class="col-lg-12">
					<img src="{{ asset('itlabil/images/default/struktur.jpg') }}">				
				</div>	
										
			</div> 
        </div>
    </section>
@endsection
