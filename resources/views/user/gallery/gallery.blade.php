@extends('layouts.user')

@section('content')
    <section id="layanan" data-stellar-background-ratio="2.5">
        <div class="container">
            <div class="row">
    
                <div class="col-md-12 col-sm-12">
                     <!-- SECTION TITLE -->
                    <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Gallery</h2>
                    </div>
                </div>
            </div>
            
            <div class="tz-gallery">

                <div class="row">
                    @foreach($gallery as $item)
                        <div class="col-sm-6 col-md-3">
                            <a class="lightbox" href="itlabil/images/gallery/{{ $item->photo }}">
                                <img src="itlabil/images/gallery/{{ $item->photo }}" alt="Park">
                            </a>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-12" align="center">
                    {{ $gallery->appends(compact('page'))->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
